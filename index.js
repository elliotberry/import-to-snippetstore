const fs = require("fs");
const FileHound = require("filehound");
const path = require("path");
var wholeArr = [];
var args = process.argv.slice(2);
const files = FileHound.create().paths(args[0]).find();
const cc = require("crypto");
var detect = require("language-detect");

files.then(async function (data) {
  for await (file of data) {
    await read(file);
  }
  fs.writeFileSync("./out.json", JSON.stringify(wholeArr));
});
function id() {
  return new Promise(function (res, rej) {
    cc.randomBytes(20, function (ex, buf) {
      res(buf.toString("hex"));
    });
  });
}
function det(fpath) {
  return new Promise(function (res, rej) {
    detect(fpath, function (err, language) {
      console.log(err); //=> null
      res(language);
    });
  });
}
async function read(file) {
  let lang = await det(file);
  let p = await path.parse(file);
  let data = await fs.readFileSync(file, "utf8");
  let keyy = await id();

  let newObj = {
    key: keyy,
    createAt: 1588900824807,
    updateAt: 1588900824807,
    copy: 0,
    tags: [],
    name: p.name,
    lang: lang,
    value: data,
    description: "",
  };
  await wholeArr.push(newObj);
}
